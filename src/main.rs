use settings::Args;
use clap::Parser;

mod settings;

fn main() {
    let args = Args::parse();
    println!("Host: {}, Port: {}", args.host, args.port)
}
