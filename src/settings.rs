use clap::Parser;
use std::string::String;

#[derive(Parser)]
pub struct Args {
    #[arg(long, help = "Host", default_value_t = String::from("0.0.0.0"))]
    pub host: String,

    #[arg(short, long, help = "Port", default_value_t = 6379)]
    pub port: u16,
}
